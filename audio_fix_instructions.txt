How to get fully working audio on XPS 9700 in Ubuntu 20.04:

Download and install dkms module:

https://launchpad.net/~canonical-hwe-team/+archive/ubuntu/pc-oem-dkms/+packages

Alternately, you can install linux-oem-20.04, it will install the 5.6 kernel with the above driver. Your system may be more stable with 5.4 kernel since it is in wide use.

Manually install sof-firmware from Arch Linux:

https://archlinux.pkgs.org/rolling/archlinux-extra-x86_64/sof-firmware-1.5.1-1-any.pkg.tar.zst.html

You will need to delete the existing "sof" and "sof-tplg" files from the Ubuntu linux-firmware package first. Note if this package gets updated from Canoncial and they don't update these, you will have to delete and then reextract. Files go into the /usr/lib/firmware/intel/sof and /usr/lib/firmware/intel/sof-tplg folders.

Manually install the ucm2 files:

http://xps17.ddns.net/sof-soundwire.zip

These go into /usr/share/alsa/ucm2/sof-soundwire. Delete the contents of the existing sof-soundwire folder (if you have it) and replace them with the ones from the archive.

Reboot. You should now have all your audio devices available. If speakers do not play, you may need to run alsamixer (non-root) from a terminal and unmute and set the mixer levels all the way up.

Many thanks to /u/isopr0p and /u/yoyomama1 for their previous work on this.

Edit: Tweak steps, add improved ucm2 files to get mic working and auto switching to headphones and bluetooth. :-)

Edit: Also confirmed working on Linux Mint 20 and Pop_OS 20.04

Edit: Canonical just pushed out a new linux-firmware and alsa-ucm-conf package, so you need to reinstall sof-firmware and the ucm2 files again after you update.
