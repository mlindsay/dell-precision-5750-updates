# Dell Precision 5750 Working Audio

### Notes:

1. Don't be plugged into a dock when you do this. It prevented Gnome from showing me the sound system, so I usually thought it wasn't working.
1. The original instructions say to install linux-oem-20.04. That usually didn't work for me.  Copying the files over was usually more reliable.
1. I couldn't get this working on Ubuntu 20.10, PoP! OS 20.04 and 20.10.
1. I DID get this working on both Ubuntu 20.04 and Mint 20.1
1. There was a lot of old info about these instructions on various sites I found. The most recent instructions I could find, that worked, are saved as `audio_fix_instructions.txt`.  This repo just tries to simplify those instructions, by including everything needed in one bundle.

### Instructions:

1. Install the 'soundwire-dkms_1.2.2_all.deb' from the file browser, by double clicking it.
1. Delete the `sof` and `sof-tplg` directories from `/usr/lib/firmware/intel` if they exist.
1. Copy the repo `sof` and `sof-tplg` directories from `sof-firmware/` into `/usr/lib/firmware/intel`. On Mint, I had to create the directory structure; `/usr/lib/firmware/intel`.
1. Delete the `/usr/share/alsa/ucm2/sof-soundwire` directory
1. Copy `sof-soundwire` directory from repo into `/usr/share/alsa/ucm2/`.
1. Reboot


#### References:
[https://www.reddit.com/r/Dell/comments/husu3r/how_to_get_fully_working_audio_in_ubuntu_2004_on/](Reddit Thread by nsx2brz)
